package InterfazGrafica;


import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Negocio.GrafoPersonas;
import Negocio.Persona;

public class Interfaz extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField nombre;
	private JTextField deporte;
	private JTextField musica;
	private JTextField espectaculo;
	private JTextField ciencia;
	public  List<Persona> Personas = new ArrayList<Persona>();

	public Interfaz() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1122, 633);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel LabelNombre = new JLabel("Nombre:");
		LabelNombre.setBounds(22, 62, 250, 14);
		contentPane.add(LabelNombre);
		
		nombre = new JTextField();
		nombre.setBounds(22, 87, 250, 20);
		contentPane.add(nombre);
		nombre.setColumns(10);
		
		JLabel LabelDeporteI = new JLabel("Interes por el deporte:");
		LabelDeporteI.setBounds(22, 134, 158, 14);
		contentPane.add(LabelDeporteI);
		
		deporte = new JTextField();
		deporte.setBounds(22, 157, 250, 20);
		deporte.setColumns(10);	    
	    TextPrompt placeholder = new TextPrompt("Ingresar un numero entre 0 y 5", deporte);
	    placeholder.changeAlpha(0.75f);	
	    placeholder.changeStyle(Font.ITALIC);
		contentPane.add(deporte);
		
		JLabel LabelMusicaI = new JLabel("Interes por la Musica:");
		LabelMusicaI.setBounds(22, 214, 187, 14);
		contentPane.add(LabelMusicaI);
		
		musica = new JTextField();
		musica.setBounds(22, 237, 250, 20);
		musica.setColumns(10);
		TextPrompt placeholder1 = new TextPrompt("Ingresar un numero entre 0 y 5", musica);
	    placeholder1.changeAlpha(0.75f);	
	    placeholder1.changeStyle(Font.ITALIC);
		contentPane.add(musica);
		
		JLabel LabelEspectaculoI = new JLabel("Interes por el Espectaculo:");
		LabelEspectaculoI.setBounds(22, 290, 187, 14);
		contentPane.add(LabelEspectaculoI);
		
		espectaculo = new JTextField();
		espectaculo.setBounds(22, 313, 250, 20);
		espectaculo.setColumns(10);
		TextPrompt placeholder2 = new TextPrompt("Ingresar un numero entre 0 y 5", espectaculo);
	    placeholder2.changeAlpha(0.75f);	
	    placeholder2.changeStyle(Font.ITALIC);
		contentPane.add(espectaculo);
		
		JLabel LabelCienciaI = new JLabel("Interes por la Ciencia:");
		LabelCienciaI.setBounds(22, 378, 187, 14);
		contentPane.add(LabelCienciaI);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(305, 65, 750, 442);
		contentPane.add(scrollPane);
		DefaultTableModel modelo = new DefaultTableModel();
		JTable tabla = new JTable(modelo);
		modelo.addColumn("Nombre");
		modelo.addColumn("Interes por el deporte");
		modelo.addColumn("Interes por la musica");
		modelo.addColumn("Interes por el espectaculo");
		modelo.addColumn("Interes por la ciencia");

		scrollPane.setViewportView(tabla);
		
		ciencia = new JTextField();		
		ciencia.setBounds(22, 401, 250, 20);
		ciencia.setColumns(10);
		TextPrompt placeholder3 = new TextPrompt("Ingresar un numero entre 0 y 5", ciencia);
	    placeholder3.changeAlpha(0.75f);	
	    placeholder3.changeStyle(Font.ITALIC);
		contentPane.add(ciencia);
		
		JButton btnAgregarPersona = new JButton("Agregar Persona");
		btnAgregarPersona.setBounds(83, 471, 126, 23);
		btnAgregarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ( Integer.parseInt(deporte.getText())>5 || Integer.parseInt(deporte.getText())<0 ||
						Integer.parseInt(musica.getText())>5 || Integer.parseInt(musica.getText())<0 ||
						Integer.parseInt(espectaculo.getText())>5 || Integer.parseInt(espectaculo.getText())<0 ||
						Integer.parseInt(ciencia.getText())>5 || Integer.parseInt(ciencia.getText())<0) {
				
					JOptionPane.showMessageDialog(null, "Los valores de interes deben estar en 0 y 5");
				}
				else {
					if (nombre.getText().equals("") || deporte.getText().equals("")
						|| musica.getText().equals("") || espectaculo.getText().equals("") ||
						ciencia.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "No puede haber campos vacios");
				}
					else {
						String[] datosFila={nombre.getText(), deporte.getText(), musica.getText(), espectaculo.getText(), ciencia.getText()};
						modelo.addRow(datosFila);
						Persona p = new Persona(nombre.getText(),Integer.parseInt(deporte.getText()) , 
						Integer.parseInt(musica.getText()),Integer.parseInt(espectaculo.getText()) , 
						Integer.parseInt(ciencia.getText()));
						Personas.add(p);
						nombre.setText("");
						deporte.setText("");
						musica.setText("");
						espectaculo.setText("");
						ciencia.setText("");
			}
		}}});
		contentPane.add(btnAgregarPersona);
		
		JButton btnGenerarGrafo = new JButton("Generar Grafo");
		btnGenerarGrafo.setBounds(500, 535, 126, 23);
		btnGenerarGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Personas.size()<1) {
					JOptionPane.showMessageDialog(null, "No hay suficientes personas para agregar al grafo");
				}
				else {
				GrafoPersonas datos = new GrafoPersonas(Personas.size());
				agregarPersonas(datos);
				datos.toString();
				datos.setearGrafo();
		    	GrafoVisual g= new GrafoVisual(datos);
				g.setVisible(true);
				}
			}
		});
		contentPane.add(btnGenerarGrafo);		
	

		JButton btnVerEstadisticas = new JButton("Ver Estadisticas");
		btnVerEstadisticas.setBounds(750, 535, 126, 23);
		btnVerEstadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Personas.size()<1) {
					JOptionPane.showMessageDialog(null, "No hay suficientes personas para calcular estadisticas");
				}else {
					GrafoPersonas datos = new GrafoPersonas(Personas.size());
					agregarPersonas(datos);
					datos.setearGrafo();
					JOptionPane.showMessageDialog(null, datos.getEstadisticas().dameEstadisticas(datos.getPersonas()));
				}}

			
			});
		contentPane.add(btnVerEstadisticas);
	}
		private void agregarPersonas(GrafoPersonas datos) {
			for (int i = 0; i < Personas.size(); i++) {
				datos.agregarPersona(Personas.get(i).getNombre(), Personas.get(i).getDeporteI(), Personas.get(i).getMusicaI(),
						Personas.get(i).getEspectaculoI() , Personas.get(i).getCienciaI(),i);
			}
	}
}


