package InterfazGrafica;

import java.awt.*;
import java.net.URL;

import javax.swing.ImageIcon;

public class Circulo{

    int x,y,n;
    Image image;
    String nombrePersona;
    URL direccion;

    public Circulo( int x, int y,String nombre) {
    	direccion=getClass().getResource("../imagenes/");        
        this.x=x;
        this.y=y;
        nombrePersona=nombre;
        image = new ImageIcon(direccion.getPath()+"esfera.png").getImage();
    }

    public void painter(Graphics g,lienzo l) {
            g.drawImage(image, x-15, y-15, l);
            g.setColor(Color.BLACK);
            g.setFont(new Font("Comic Sans MS", Font.BOLD, 22));
            String n = nombrePersona.substring(0, 1).toUpperCase() + nombrePersona.substring(1);
            g.drawString(n, x+10, y-14);
        }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getN() {
        return n;
    }
    
    public void transladar(int dx,int dy) {
        this.x+=dx; this.y+=dy;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public boolean desplazadoPor(Point d) {
        if(d.distance(x, y)<=15) {
            return true;
        }
         else {
                return false;
         }
    }
}
