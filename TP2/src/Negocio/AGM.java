package Negocio;

public class AGM {
	private int padre[];
	private int[][] adyacencia;
	private int vertices;	
	
	public AGM(Grafo grafo) {				
		this.adyacencia = grafo.getAdyacencia();		
		this.vertices= grafo.getVertices();
		this.padre=new int[grafo.getVertices()];		
	}

	public Grafo arbolMinimo() {
		Grafo agm= new Grafo(vertices);
		int[][] arbol= agm.getAdyacencia();
		inicializarPadre(vertices);			
		int verticeA=-1;
	    int verticeB=-1;	    
	    int arista = 1;	    
	    while(arista < vertices){
	        // Encontrar  arista mnima que no forma ciclo y guardar los vertices y el peso.
	        Integer min = 20;
	        for(int i = 0; i < vertices; i++) {
	            for(int j = 0; j < vertices; j++) {
	                if(min > adyacencia[i][j] && adyacencia[i][j]!=-1 && !compararComponentesConexas(i,j)){
	                    min = adyacencia[i][j];
	                    verticeA = i;
	                    verticeB = j;
	                }
	            }
	        }	        
	                // Si los vertices no pertenecen al mismo arbol agrego la arista al arbol minimo.
	        if(!compararComponentesConexas(verticeA,verticeB)){
	            arbol[verticeA][verticeB] = min;
	            arbol[verticeB][verticeA] = min;

	            // Todos los vertices del arbol del nodoB ahora pertenecen al arbol del nodoA.
	        	int temp = Find(verticeB);
	        	Union(verticeB, verticeA);
	        	for(int k = 0; k < vertices; k++) {
	        		if(padre[k] == temp) {
	        			padre[k] = padre[verticeA];
	        		}
	        	}
	            arista++;
	        }
	    }
    	agm.setAdyacencia(arbol);
    	return agm;
	   
	}


	//UNION-FIND	
	public void inicializarPadre( int n){
	    for( int i = 1 ; i < n ; ++i ) {
	    	padre[i] = i;
	    	}
	}
	
	public int Find( int x ){
	    return ( x == padre[ x ] ) ? x : ( padre[ x ] = Find( padre[ x ] ) );
	}
	
	public void Union( int x , int y ){
	    padre[ Find( x ) ] = Find( y );
	}

	
	public boolean compararComponentesConexas( int x , int y ){
	    if( Find( x ) == Find( y ) ) return true;
	    return false;
	}
	//UNION-FIND
	

	public int[][] getAdyacencia() {
		return adyacencia;
	}

	public void setAdyacencia(int[][] adyacencia) {
		this.adyacencia = adyacencia;
	}
	
}