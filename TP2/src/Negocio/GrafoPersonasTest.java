package Negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class GrafoPersonasTest {

	@Test
    public void agregarPersonaTest() {
        GrafoPersonas g = new GrafoPersonas(1);
        g.agregarPersona("a", 1, 2, 3, 4, 0);
        Persona a = new Persona("a", 1, 2, 3, 4);
        Persona p[] = { a };
        assertEquals(g.getPersonas(), p);
    }

	@Test
    public void setearGrafoTest() {
        GrafoPersonas g = new GrafoPersonas(3);
        g.agregarPersona("a", 1, 2, 3, 4, 0);
        g.agregarPersona("b", 2, 3, 5, 4, 1);
        g.agregarPersona("c", 3, 2, 1, 4, 2);
        g.setearGrafo();
        int[][] a = { { -1, 4, 4 }, { 4, -1, 6 }, { 4, 6, -1 } };
        assertEquals(g.getGrafo().getAdyacencia(), a);

    }

    @Test
    public void indiceSimilaridadTest() {
        GrafoPersonas g = new GrafoPersonas(2);
        Persona a = new Persona("a", 1, 2, 3, 4);
        Persona b = new Persona("b", 2, 3, 4, 5);
        assertEquals(g.indiceDeSimilaridad(a, b), 4);
    } 

    @Test
    public void dividirTest() {
        GrafoPersonas g = new GrafoPersonas(3);
        g.agregarPersona("a", 1, 2, 3, 4, 0);
        g.agregarPersona("b", 2, 3, 5, 4, 1);
        g.agregarPersona("c", 3, 2, 1, 4, 2);
        g.setearGrafo();
        g.dividir();
        int[][] a = { { -1, 4, 4 }, { 4, -1, -1 }, { 4, -1, -1 } };
        assertEquals(g.getGrafo().getAdyacencia(), a);
    }
}
