package Negocio;

public class Persona {
	private String nombre;
	private int deporteI;
	private int musicaI;
	private int espectaculoI;
	private int cienciaI;
	
	public Persona(String nombre, int deporteI, int musicaI, int espectaculoI, int cienciaI) {
		this.nombre = nombre;			
		this.deporteI = deporteI;
		this.musicaI = musicaI;
		this.espectaculoI = espectaculoI;
		this.cienciaI = cienciaI;
	}

	@Override
	public String toString() {
		return "[" + nombre + ", deporteI=" + deporteI + ", musicaI=" + musicaI + ", espectaculoI="
				+ espectaculoI + ", cienciaI=" + cienciaI + "]";
	}	

	@Override
	public boolean equals(Object obj) {				
		Persona other = (Persona) obj;
		if (cienciaI != other.cienciaI || deporteI != other.deporteI || espectaculoI != other.espectaculoI 
				|| musicaI != other.musicaI || !nombre.equals(other.nombre)) {
			return false;
			}
		return true;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setDeporteI(int deporteI) {
		this.deporteI = deporteI;
	}

	public void setMusicaI(int musicaI) {
		this.musicaI = musicaI;
	}

	public void setEspectaculoI(int espectaculoI) {
		this.espectaculoI = espectaculoI;
	}

	public void setCienciaI(int cienciaI) {
		this.cienciaI = cienciaI;
	}

	public String getNombre() {
		return nombre;
	}

	public int getDeporteI() {
		return deporteI;
	}

	public int getMusicaI() {
		return musicaI;
	}

	public int getEspectaculoI() {
		return espectaculoI;
	}

	public int getCienciaI() {
		return cienciaI;
	}
	

	

}
